"~/.config/nvim/init.vim
call plug#begin()

Plug 'stephpy/vim-yaml'
Plug 'rust-lang/rust.vim'

call plug#end()

set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab encoding=utf-8 syntax=on

let g:rustfmt_autosave=1
set clipboard=unnamed
